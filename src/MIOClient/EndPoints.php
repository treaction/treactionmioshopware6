<?php

namespace Treaction\MIO\MIOClient;

class EndPoints
{
    // Host Name of the AIO
    public const END_POINT_HOST = "https://api-in-one.net";

    // Supported Services of the plugin.
    public const WEBHOOK_LIST_READ = self::END_POINT_HOST . "/v2.0/webhook/list/read";
    public const WEBHOOK_RECEIVE = self::END_POINT_HOST . "/v2.0/webhook/receive";
    public const VALIDATE_APIKEY = self::END_POINT_HOST . "/v2.0/general/validate/apikey";
    public const CONTACTS_SYNC = self::END_POINT_HOST . "/v2.0/contacts/sync";
    public const ACCOUNT_CREATE_ENDPOINT = self::END_POINT_HOST . "/v2.0/general/account/create";

    // Supported Methods
    public const METHOD_POST = "POST";
    public const METHOD_GET = "GET";
}