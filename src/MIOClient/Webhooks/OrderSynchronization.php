<?php

namespace Treaction\MIO\MIOClient\Webhooks;

use Treaction\MIO\MIOClient\EndPoints;
use Treaction\MIO\MIOClient\Utils;
use Treaction\MIO\MIOClient\WebServices\CURLService;
use Treaction\MIO\Service\PluginLogger;

class OrderSynchronization extends AbstractWebhook
{

    public function send(array $orders): bool
    {
        $chunks = array_chunk($orders, 100);
/*        $this->logger->addLog('info', 'Chunks', json_encode($chunks));
        $this->logger->addLog('info', 'TotalNumberOfChunks', json_encode(count($chunks)));*/
        $basePayload = $this->getBasePayload();
        if($basePayload === null) {
            return false;
        }
        $start = microtime(true);
        $payload[ 'base' ] = $basePayload;
        foreach ($chunks as $chunk) {
            foreach ($chunk as $orderChunk) {
                $payload[ 'contacts' ][]= $this->generatePayload($orderChunk);
            }
            $body = $this->utils->simpleEncode($payload);
/*            $this->logger->addLog('info', 'orderSync', json_encode($body));*/
            $response = $this->cURLService->sendRequest($body, $this->getMethod(), $this->getEndPoint());
            unset($payload[ 'contacts' ]);
        }
        $time_elapsed_secs = microtime(true) - $start;
        $this->logger->addLog('info', 'Time excution '.json_encode($time_elapsed_secs));
        return true;
    }

    /**
     * @return array
     * @author Pradeep
     */
    private function getBasePayload(): ?array
    {
        $apikey = $this->getAPIKey();
        $accountNumber = $this->getAccountNumber();
        $objectRegisterId = $this->getObjectRegisterId(self::ECHOOK);
        if($apikey === null || $accountNumber === null || $objectRegisterId === null) {
            return null;
        }
        return [
            'apikey' => $apikey,
            'account_number' => $accountNumber,
            'object_register_id' =>$objectRegisterId,
        ];
    }

    /**
     * @param array $payload
     * @return array
     * @author Pradeep
     */
    public function generatePayload(array $payload): array
    {
        if (empty($payload)) {
            return [];
        }
        $contact[ 'standard' ] = [
            [
                'email' => $payload[ 'email' ] ?? '',
                'required' => '',
                'datatype' => 'Email',
                'regex' => '',
            ],
            [
                'salutation' => $payload[ 'salutation' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'first_name' => $payload[ 'firstName' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_name' => $payload[ 'lastName' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'street' => $payload[ 'street' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'house_number' => $payload[ 'hNo' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'postal_code' => $payload[ 'postalCode' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'city' => $payload[ 'city' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'country' => $payload[ 'country' ] ?? '',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'smart_tags' => $payload[ 'smartTags' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'total_order_net_value' => $payload[ 'totalOrderNetValue' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_date' => $payload[ 'lastOrderDate' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_no' => $payload[ 'lastOrderNumber' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_order_net_value' => $payload[ 'lastOrderNetValue' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
            [
                'last_year_order_net_value' => $payload[ 'lastYearOrderNetValue' ] ?? 'empty',
                'required' => '',
                'datatype' => 'Text',
                'regex' => '',
            ],
        ];
        return $contact;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getEndPoint(): string
    {
        return EndPoints::CONTACTS_SYNC;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getMethod(): string
    {
        return EndPoints::METHOD_POST;
    }

}