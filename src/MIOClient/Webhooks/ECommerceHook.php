<?php

namespace Treaction\MIO\MIOClient\Webhooks;


use Treaction\MIO\MIOClient\EndPoints;

class ECommerceHook extends AbstractWebhook
{
    public function send(array $payload): bool
    {
        $payload = $this->generatePayload($payload);
        $this->logger->addLog('info ','generatePayload'.json_encode($payload));
        $body = $this->utils->simpleEncode($payload);
        $this->logger->addLog('info ','ECHook send '.json_encode($body));
        $response = $this->cURLService->sendRequest($body,$this->getMethod(),$this->getEndPoint());
        if(isset($response['status'], $response['message'])) {
            $this->logger->addLog('info ', 'ECHook '.json_encode($response));
        }
        return $response['status'];
    }

    public function generatePayload(array $payload): array
    {
        return [
            'base' => [
                'apikey' => $this->getAPIKey(),
                'account_number' => $this->getAccountNumber(),
                'object_register_id' => $this->getObjectRegisterId(self::ECHOOK),
            ],
            'contact' => [
                'standard' => [
                    [
                        'email' => $payload['email'] ?? '-',
                        'required' => '',
                        'datatype' => 'Email',
                        'regex' => '',
                    ],
                    [
                        'salutation' => $payload['salutation'] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'first_name' => $payload['firstName'] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_name' => $payload['lastName'] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'street' => $payload[ 'street' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'house_number' =>$payload[ 'hNo' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'postal_code' => $payload[ 'postalCode' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'city' => $payload[ 'city' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'country' => $payload[ 'country' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'smart_tags' => $payload['smartTags'] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'total_order_net_value' => $payload['totalOrderNetValue'] ?? '0.00',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_date' => $payload[ 'lastOrderDate' ] ?? '-',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_no' => $payload[ 'lastOrderNumber' ] ?? '00000',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_order_net_value' => $payload[ 'lastOrderNetValue' ] ?? '0.00',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_year_order_net_value' => $payload['lastYearOrderNetValue'] ?? '0.00',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => ''
                    ],
                ],
                'custom' => '',
            ],
        ];
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getEndPoint(): string
    {
        return EndPoints::WEBHOOK_RECEIVE;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getMethod():string
    {
        return EndPoints::METHOD_POST;
    }
}