<?php

namespace Treaction\MIO\MIOClient\Webhooks;


use Treaction\MIO\MIOClient\EndPoints;
use Treaction\MIO\MIOClient\Utils;
use Treaction\MIO\MIOClient\WebServices\CURLService;
use Treaction\MIO\Service\PluginLogger;

abstract class AbstractWebhook
{
    public const NLHOOK = 'NewsletterHook';
    public const ECHOOK = 'eCommerceHook';

    /**
     * @var string
     * @author Pradeep
     */
    private $apiKey;
    /**
     * @var int
     * @author Pradeep
     */
    private $accountNumber;
    /**
     * @var int
     * @author Pradeep
     */
    private $objectRegisterId;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    protected $logger;
    /**
     * @var Utils
     * @author Pradeep
     */
    protected $utils;
    /**
     * @var CURLService
     * @author Pradeep
     */
    protected $cURLService;

    abstract public function send(array $payload) : bool;
    abstract public function generatePayload(array $payload):array;
    abstract public function getEndPoint():string;

    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->utils = new Utils();
        $this->cURLService = new CurlService();
    }

    /**
     * @param string $apiKey
     * @return bool
     * @author Pradeep
     */
    public function setAPIKey(string $apiKey): bool
    {
        $status = false;
        if(!empty($apiKey)) {
            $status = true;
            $this->apiKey = $apiKey;
        }
        return $status;
    }

    /**
     * @param int $accountNumber
     * @return bool
     * @author Pradeep
     */
    public function setAccountNumber(int $accountNumber): bool
    {
        $status = false;
        if($accountNumber > 0) {
            $status = true;
            $this->accountNumber = $accountNumber;
        }
        return $status;
    }

    /**
     * @param int $objectRegisterId
     * @return bool
     * @author Pradeep
     */
    public function setObjectRegisterId(int $objectRegisterId): bool
    {
        $status = false;
        if($objectRegisterId <= 0) {
            $status = true;
            $this->objectRegisterId = $objectRegisterId;
        }
        return $status;
    }

    /**
     * @return string
     * @author Pradeep
     */
    protected function getAPIKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function getAccountNumber(): ?int
    {
        return $this->accountNumber;
    }

    /**
     * @param string $webhookType
     * @return int|null
     * @author Pradeep
     */
    public function getObjectRegisterId(string $webhookType): ?int
    {
        $objectregister_id = null;
        $webhooks = $this->getWebhooks();

        if (empty($webhooks) || !$webhooks[ 'status' ] || empty($webhooks[ 'response' ])) {
            return null;
        }
        foreach ($webhooks[ 'response' ] as $webhook) {
            if ($webhook[ 'name' ] !== $webhookType || empty($webhook[ 'objectregister_id' ])) {
                continue;
            }
            $objectregister_id = $webhook[ 'objectregister_id' ];
        }
        return $objectregister_id;
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getWebhooks(): array
    {
        $webhooks = [];
        $method = EndPoints::METHOD_POST;
        $apikey = $this->getAPIKey();
        $accountNo = $this->getAccountNumber();
        $endPoint = EndPoints::WEBHOOK_LIST_READ;
        $body = [
            'base' => [
                'account_no' => $accountNo,
                'apikey' => $apikey,
            ],
        ];
        // AS PER THE SERVICE FROM AIO (data IS APPENDED TO THE BODY)
        $payload = $this->utils->simpleEncode($body);
        $base64Text = 'data=' . $payload;
        return $this->cURLService->sendRequest($base64Text, $method, $endPoint, ['Content-Type: application/x-www-form-urlencoded']);
    }
}