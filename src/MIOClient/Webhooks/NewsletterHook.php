<?php

namespace Treaction\MIO\MIOClient\Webhooks;

use Treaction\MIO\MIOClient\EndPoints;

class NewsletterHook extends AbstractWebhook
{

    /**
     * @param array $payload
     * @return bool
     * @author Pradeep
     */
    public function send(array $payload): bool
    {
        // TODO: Implement send() method.
        $payload = $this->generatePayload($payload);
        $this->logger->addLog('info ','generatePayload'.json_encode($payload));
        $body = $this->utils->simpleEncode($payload);
        $this->logger->addLog('info ','NLHook send '.json_encode($body));
        $time_start = microtime(true);
        $response = $this->cURLService->sendRequest($body,$this->getMethod(),$this->getEndPoint());
        if(isset($response['status'], $response['message'])) {
            $this->logger->addLog('info ', 'Newsletter Status '.json_encode($response));
        }
        $time_diff =$time_start -  microtime(true);
        $this->logger->addLog('info ','Time taken '.json_encode($time_diff));
        return $response['status'];
    }

    /**
     * @param array $payload
     * @return array[]
     * @author Pradeep
     */
    public function generatePayload(array $payload): array
    {
        $this->logger->addLog('info ','Inside generatePayload '.json_encode($payload));
        return [
            'base' => [
                'apikey' => $this->getAPIKey(),
                'account_number' => $this->getAccountNumber(),
                'object_register_id' => $this->getObjectRegisterId(self::NLHOOK),
            ],
            'contact' => [
                'standard' => [
                    [
                        'email' => $payload['email'] ?? '',
                        'required' => '',
                        'datatype' => 'Email',
                        'regex' => '',
                    ],
                    [
                        'salutation' => $payload['salutation'] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'first_name' => $payload['firstName'] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                    [
                        'last_name' => $payload['lastName'] ?? '',
                        'required' => '',
                        'datatype' => 'Text',
                        'regex' => '',
                    ],
                ],
                'custom' => '',
            ],
        ];
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getEndPoint(): string
    {
        return EndPoints::WEBHOOK_RECEIVE;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getMethod():string
    {
        return EndPoints::METHOD_POST;
    }
}