<?php

namespace Treaction\MIO\MIOClient\Account;

use Treaction\MIO\MIOClient\EndPoints;

class UserAccount extends BaseAccount
{

    /**
     * @param string $salutation
     * @param string $firstName
     * @param string $lastName
     * @param string $eMail
     * @param string $permission
     * @return array
     * @author Pradeep
     */
    public function create(
        string $salutation,
        string $firstName,
        string $lastName,
        string $eMail,
        string $permission
    ): array {
        $data = [
            'salutation' => $salutation,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $eMail,
            'permission' => $permission,
        ];

        $payload = $this->utils->simpleEncode($data);
        $response = $this->cURLService->sendRequest($payload, 'POST', EndPoints::ACCOUNT_CREATE_ENDPOINT);
        $this->logger->addLog('Info', json_encode($response), __CLASS__, __METHOD__, __LINE__);
        return $response;

    }
}