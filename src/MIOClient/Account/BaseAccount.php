<?php

namespace Treaction\MIO\MIOClient\Account;

use Treaction\MIO\MIOClient\Utils;
use Treaction\MIO\MIOClient\WebServices\CURLService;
use Treaction\MIO\Service\PluginLogger;

abstract class BaseAccount
{
    /**
     * @var CURLService
     * @author Pradeep
     */
    protected $cURLService;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    protected $logger;
    /**
     * @var Utils
     * @author Pradeep
     */
    protected $utils;

    public function __construct()
    {
        $this->cURLService = new CurlService();
        $this->logger = new PluginLogger();
        $this->utils = new Utils();
    }
}