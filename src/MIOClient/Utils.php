<?php

namespace Treaction\MIO\MIOClient;

use Treaction\MIO\Service\PluginLogger;

class Utils
{

    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;

    public function __construct()
    {
        $this->logger = new PluginLogger();
    }

    public function simpleEncode(array $arrayToEncode):?string
    {
        $encodedString = null;
        if(empty($arrayToEncode)) {
            return null;
        }
        $jsonStr = $this->toJson($arrayToEncode);
        return base64_encode($jsonStr);
    }

    public function simpleDecode(array $arrayToDecode):?array
    {
        $decodedString = null;
        if(empty($arrayToDecode)) {
            return null;
        }
        $jsonStr = base64_decode($arrayToDecode);
        return $this->toArray($jsonStr);
    }

    public function toJson(array $arrToJson)
    {
        return json_encode($arrToJson);
    }

    public function toArray(string $jsonStr)
    {
        return json_decode($jsonStr, true);
    }

}