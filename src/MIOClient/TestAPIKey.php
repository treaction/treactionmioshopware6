<?php

namespace Treaction\MIO\MIOClient;

use Treaction\MIO\MIOClient\WebServices\CURLService;
use Treaction\MIO\Service\PluginLogger;

class TestAPIKey
{
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var Utils
     * @author Pradeep
     */
    private $utils;
    /**
     * @var CURLService
     * @author Pradeep
     */
    private $cURLService;

    public function __construct()
    {
        $this->logger = new PluginLogger();
        $this->utils = new Utils();
        $this->cURLService = new CurlService();
    }

    /**
     * @param string $apiKey
     * @param int $accountNumber
     * @return array|null
     * @author Pradeep
     */
    public function validateAPIKeyAccountNumber(string $apiKey, int $accountNumber): ?array
    {
        if (empty($apiKey) || empty($accountNumber)) {
            return [];
        }
        $method = EndPoints::METHOD_POST;
        $endPoint = EndPoints::VALIDATE_APIKEY;
        $body = [
            'base' => [
                'account_number' => $accountNumber,
                'apikey' => $apiKey,
            ],
        ];

        $payload = $this->utils->simpleEncode($body);
        return $this->cURLService->sendRequest($payload, $method, $endPoint,
            ['Content-Type: application/x-www-form-urlencoded']);
    }
}