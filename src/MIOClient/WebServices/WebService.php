<?php
namespace Treaction\MIO\MIOClient\WebServices;

Interface WebService
{
    public function sendRequest(string $data, string $method, string $endPoint, array $headers = []);
    public function endPoint();
    public function payLoad();
}