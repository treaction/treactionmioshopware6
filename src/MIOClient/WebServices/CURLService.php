<?php
namespace Treaction\MIO\MIOClient\WebServices;

use Treaction\MIO\MIOClient\EndPoints;
use Treaction\MIO\MIOClient\Utils;
use Treaction\MIO\Service\PluginLogger;

class CURLService implements WebService
{

    /**
     * @var false|resource
     * @author Pradeep
     */
    private $curl;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var Utils
     * @author Pradeep
     */
    private $utils;

    public function __construct()
    {
        $this->curl = curl_init();
        $this->logger = new PluginLogger();
        $this->utils = new Utils();
    }

    /**
     * @param string $method
     * @return bool
     * @author Pradeep
     */
    private function isValidMethod(string $method):bool
    {
        $post = EndPoints::METHOD_POST;
        $get = EndPoints::METHOD_GET;
        if(!in_array($method, [$post, $get, strtoupper($post), strtoupper($get)], true)) {
            $this->logger->addLog('error','Invalid Method provided',__CLASS__,__METHOD__,__LINE__);
            return false;
        }
        return true;
    }

    /**
     * @param string $endPoint
     * @return bool
     * @author Pradeep
     */
    private function isValidEndPoint(string $endPoint):bool
    {
        if(!filter_var($endPoint, FILTER_VALIDATE_URL)){
            $this->logger->addLog('error','Invalid Endpoint URL provided',__CLASS__,__METHOD__,__LINE__);
            return false;
        }
        return true;
    }


    /**
     * @param string $data
     * @param string $method
     * @param string $endPoint
     * @return bool
     * @author Pradeep
     */
    private function setOptions(string $data, string $method, string $endPoint, array $headers =[]):bool
    {
        if(!$this->isValidEndPoint($endPoint) || !$this->isValidMethod($method)){
            return false;
        }
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $endPoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
            CURLOPT_HTTPHEADER => $this->getHeaders($headers)
        ));
        $this->logger->addLog('info','cURL Options : '.json_encode($this->getHeaders($headers)),__CLASS__,__METHOD__,__LINE__);
        return true;
    }

    /**
     * @param string $data
     * @param string $method
     * @param string $endPoint
     * @param array $headers
     * @return array|null
     * @author Pradeep
     */
    public function sendRequest(string $data, string $method, string $endPoint, array $headers = [])
    {
        // TODO: Implement sendRequest() method.
        if(!$this->setOptions($data, $method, $endPoint, $headers)) {
            $this->logger->addLog('error','Failed to set cURL Options',__CLASS__,__METHOD__,__LINE__);
            return [
                'status' => false,
                'message' => 'Invalid Options provided'
            ];
        }
        $response = $this->exec();
        $error = $this->getError();
        $this->logger->addLog('info','cURL error '.json_encode($error),__CLASS__,__METHOD__,__LINE__);
        $this->logger->addLog('info','curlResponse '.json_encode($response),__CLASS__,__METHOD__,__LINE__);
        return $this->parseToArray($response);
    }

    /**
     * @param string $jsonString
     * @return array|null
     * @author Pradeep
     */
    public function parseToArray(string $jsonString):?array
    {
        if(empty($jsonString)) {
            return [];
        }
        return json_decode($jsonString, true);
    }

    /**
     * @param array $headers
     * @return array|string[]
     * @author Pradeep
     */
    private function getHeaders(array $headers=[]):array
    {
        $defaultHeaders = ['Content-Type: application'];
        if(empty($headers)) {
            return $defaultHeaders;
        }
        return $headers;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function exec():string
    {
        if(empty($this->curl)) {
            $this->logger->addLog('error','Empty CURL ',__CLASS__,__METHOD__,__LINE__);
            return '';
        }
        return curl_exec($this->curl);
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getError():string
    {
        if(empty($this->curl)) {
            $this->logger->addLog('error','Empty CuRL',__CLASS__,__METHOD__,__LINE__);
            return '';
        }
        return curl_error($this->curl);
    }

    public function endPoint()
    {
        // TODO: Implement endPoint() method.
    }

    public function payLoad()
    {
        // TODO: Implement payLoad() method.
    }
}