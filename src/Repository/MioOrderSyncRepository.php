<?php

namespace Treaction\MIO\Repository;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use Treaction\MIO\Service\PluginLogger;

class MioOrderSyncRepository
{
    public function __construct(
        PluginLogger $logger,
        Connection $connection
    ) {
        $this->logger = $logger;
        $this->connection = $connection;
    }

    public function insert(
        int $mioAccountNumber,
        int $firstOrderId,
        int $lastOrderId,
        bool $syncStatus,
        int $ordersCount
    ): void {
        $this->connection->insert('mio_orders_sync', [
            'id' => Uuid::randomHex(),
            'mio_account_number' => (string)$mioAccountNumber,
            'last_sync_time_stamp' => (new DateTimeImmutable())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            'last_sync_status' => $syncStatus,
            'last_order_id' => $lastOrderId,
            'first_order_id' => $firstOrderId,
            'orders_count' => $ordersCount,
        ]);
    }

    public function get(int $mioAccountNumber)
    {
        return $this->connection->fetchAll(
            '
                SELECT * 
                FROM `mio_orders_sync` 
                WHERE `mio_account_number` = '. $mioAccountNumber.' 
                ORDER BY `last_order_id` DESC
                LIMIT 1');
    }

}