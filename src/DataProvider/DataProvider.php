<?php

namespace Treaction\MIO\DataProvider;

use Shopware\Core\Framework\Context;

abstract class DataProvider
{

    /**
     * @var Context
     * @author Pradeep
     */
    protected $context;

    abstract public function getEntity();

    public function setContext(Context $context):void
    {
        $this->context = $context;
    }

    public function getContext():?Context
    {
        return $this->context;
    }

}