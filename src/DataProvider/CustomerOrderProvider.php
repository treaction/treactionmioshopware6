<?php

namespace Treaction\MIO\DataProvider;

use Doctrine\DBAL\Connection;
use Shopware\Core\Checkout\Order\Aggregate\OrderCustomer\OrderCustomerEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Treaction\MIO\Service\PluginLogger;

class CustomerOrderProvider extends DataProvider
{

    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $customerOrderRepository;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $connection;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;

    public function __construct(EntityRepositoryInterface $customerOrderRepository, Connection $connection)
    {
        $this->customerOrderRepository = $customerOrderRepository;
        $this->connection = $connection;
        $this->logger = new PluginLogger();
    }
    /**
     * @var
     * @author Pradeep
     */
    protected $context;

    public function getEntity()
    {
        return 'order.customer';
    }


    public function getUserInfo(OrderCustomerEntity $orderCustomerEntity)
    {
        $customer['lastName'] = $orderCustomerEntity->getLastName();
        $customer['firstName'] = $orderCustomerEntity->getFirstName();
        $customer[ 'email' ] = $orderCustomerEntity->getEmail();
        $customer[ 'salutation' ] = $orderCustomerEntity->getSalutation()->getDisplayName();
        return $customer;
    }

    public function getLastOrderDate(OrderCustomerEntity $orderCustomerEntity)
    {
        return $orderCustomerEntity->getCustomer()->getLastOrderDate();
    }

    public function getAddress(OrderCustomerEntity $orderCustomerEntity)
    {
        $addresses = $orderCustomerEntity->getCustomer()->getAddresses();
        foreach($addresses as $address) {
            $contact['street'] = $address->getStreet();
            $contact['zipcode'] = $address->getZipcode();
            $contact['city'] = $address->getCity();
            $contact['country'] = $address->getCountry()->getName();
        }
        return $contact;
    }

}
