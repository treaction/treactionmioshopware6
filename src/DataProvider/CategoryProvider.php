<?php

namespace Treaction\MIO\DataProvider;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Treaction\MIO\Service\PluginLogger;

class CategoryProvider extends DataProvider
{

    public function __construct(EntityRepositoryInterface $categoryRepository, Connection $connection)
    {
        $this->categoryRepository = $categoryRepository;
        $this->connection = $connection;
        $this->logger = new PluginLogger();
    }

    public function getEntity()
    {
        return 'category';
    }

    public function getCategory()
    {
        $criteria = new Criteria(['2a641d87df2b47c88a69c478c7244c52']);
        $result = $this->categoryRepository
            ->search($criteria, $this->context)
            ->first();
        $this->logger->addLog('info', 'category :', json_encode($result));
    }

}
