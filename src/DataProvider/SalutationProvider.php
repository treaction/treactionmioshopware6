<?php

namespace Treaction\MIO\DataProvider;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\Salutation\SalutationEntity;
use Treaction\MIO\Service\PluginLogger;

class SalutationProvider extends DataProvider
{

    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $salutationRepository;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;

    public function __construct(EntityRepositoryInterface $salutationRepository)
    {
        $this->salutationRepository = $salutationRepository;
        $this->logger = new PluginLogger();
    }

    public function getEntity()
    {
        // TODO: Implement getEntity() method.
        return 'salutation';
    }

    public function get(?string $id):?string
    {
        if($id === null) {
            return '';
        }
        return $this->search($id);
    }

    public function search($salutationId)
    {

        $salutations = $this->salutationRepository->search(new Criteria(), $this->getContext());

        /** @var SalutationEntity $salutation */
        foreach ($salutations as $salutation) {
            if ($salutation->getId() === $salutationId) {
                if($salutation->getSalutationKey() === 'mr') {
                    return 'Herr';
                }

                if ($salutation->getSalutationKey() === 'mrs'){
                    return 'Frau';
                }

                return '';

            }
        }
        return null;
    }
}