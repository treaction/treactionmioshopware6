<?php

namespace Treaction\MIO\DataProvider;

use DateTime;
use Doctrine\DBAL\Connection;
use Shopware\Core\Checkout\Cart\Exception\OrderNotFoundException;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Treaction\MIO\Service\PluginLogger;

class OrderProvider extends DataProvider
{

    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var EntitySearchResult
     * @author Pradeep
     */
    private $lastOrderInfo;
    /**
     * @var OrderEntity
     * @author Pradeep
     */
    private $customerOrders;
    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $orderRepository;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $connection;
    /**
     * @var int
     * @author Pradeep
     */
    private $customerId;

    public function __construct(EntityRepositoryInterface $orderRepository, Connection $connection)
    {
        $this->orderRepository = $orderRepository;
        $this->connection = $connection;
        $this->logger = new PluginLogger();
    }

    public function getEntity(): string
    {
        return 'order';
    }

    /**
     * @param int $customerId
     * @return array|null
     * @author Pradeep
     */
    public function getUserInformation(int $customerId): ?array
    {
        $userInfo = [];
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntity = $customerOrders->first();
        $this->logger->addLog('info', 'orderEntity', json_encode($orderEntity));
        $userInfo[ 'email' ] = $orderEntity->getOrderCustomer()->getEmail();
        $userInfo[ 'firstName' ] = $orderEntity->getOrderCustomer()->getFirstName();
        $userInfo[ 'lastName' ] = $orderEntity->getOrderCustomer()->getLastName();
        $userInfo[ 'salutation' ] = $orderEntity->getOrderCustomer()->getSalutation()->getDisplayName();
        $addresses = $orderEntity->getAddresses();
        foreach ($addresses as $address) {
            if (empty($address)) {
                continue;
            }
            $userInfo[ 'street' ] = $address->getStreet();
            $userInfo[ 'hNo' ] = '';
            $userInfo[ 'city' ] = $address->getCity();
            $userInfo[ 'postalCode' ] = $address->getZipcode();
            $userInfo[ 'country' ] = $address->getCountry()->getName();
        }

        return $userInfo;
    }

    /**
     * @param int $customerId
     * @return OrderEntity|null
     * @author Pradeep
     */
    private function getCustomerOrders(int $customerId): ?EntitySearchResult
    {
        if( $this->getCustomerId() === null) {
            $this->setCustomerId($customerId);
        }
        if ($this->customerOrders === null || $this->getCustomerId() !== $customerId) {
            $this->setCustomerId($customerId);
            $customerOrders = $this->getOrderHistory($customerId);
            if ($customerOrders !== null) {
                $this->setCustomerOrders($customerOrders);
                return $customerOrders;
            }
            return null;
        }
        return $this->customerOrders;
    }

    /**
     * @param int $customerId
     * @return EntitySearchResult|null
     * @author Pradeep
     */
    // Todo : rename to gerOrderHistoryForSingleCustomer.
    public function getOrderHistory(int $customerId): ?EntitySearchResult
    {
        $context = $this->getContext();
        if ($context === null) {
            return null;
        }
        $criteria = new Criteria();
        $criteria->addAssociation('lineItems');
        $criteria->addAssociation('lineItems.product');
        $criteria->addAssociation('lineItems.product.cover.media.url');
        $criteria->addAssociation('lineItems.product.categories');
        $criteria->addAssociation('orderCustomer');
        $criteria->addAssociation('orderCustomer.customer');
        $criteria->addAssociation('orderCustomer.salutation');
        $criteria->addAssociation('addresses');
        $criteria->addAssociation('addresses.country');

        $criteria->addFilter(new EqualsFilter('orderCustomer.customerNumber', $customerId));
        $criteria->addSorting(new FieldSorting('order.orderNumber', FieldSorting::DESCENDING));

        return $this->orderRepository
            ->search($criteria, $context);
    }

    /**
     * @return int|null
     * @author Pradeep
     */
    public function getRecentShopOrderNumber():?int {
        $context = $this->getContext();
        if ($context === null) {
            return null;
        }

        $criteria = new Criteria();
        $criteria->addSorting(new FieldSorting('order.orderNumber', FieldSorting::DESCENDING));
        $criteria->setLimit(1);
        $searchEntity = $this->orderRepository->search($criteria, $context);
        $orders = $searchEntity->getElements();
        if(empty($orders)) {
            return null;
        }
        foreach ($orders as $order) {
            if(empty($order)) {
                continue;
            }
            $orderNumber = (int)$order->getOrderNumber();
        }

        return $orderNumber;
    }

    public function getShopOrderHistory(int $orderNumber =0) :?EntitySearchResult
    {
        $context = $this->getContext();
        if ($context === null) {
            return null;
        }

        $criteria = new Criteria();
        $criteria->addAssociation('lineItems');
        $criteria->addAssociation('lineItems.product');
        $criteria->addAssociation('lineItems.product.cover.media.url');
        $criteria->addAssociation('lineItems.product.categories');
        $criteria->addAssociation('orderCustomer');
        $criteria->addAssociation('orderCustomer.customer');
        $criteria->addAssociation('orderCustomer.salutation');
        $criteria->addAssociation('addresses');
        $criteria->addAssociation('addresses.country');
        if($orderNumber > 0 ) {
            $criteria->addFilter(new RangeFilter(
                'order.orderNumber',
                [
                    RangeFilter::GT => $orderNumber,
                ]
            ));
        }
        $criteria->addSorting(new FieldSorting('order.orderNumber', FieldSorting::ASCENDING));

        return $this->orderRepository
            ->search($criteria, $context);
    }

    /**
     * @param EntitySearchResult $customerOrders
     * @author Pradeep
     */
    private function setCustomerOrders(EntitySearchResult $customerOrders): void
    {
        $this->customerOrders = $customerOrders;
    }

    private function setCustomerId(int $customerId): void
    {
        $this->customerId = $customerId;
    }

    public function getCustomerId(): ?int {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return string|null
     * @author Pradeep
     */
    public function getLastOrderData(int $customerId): ?string
    {
        $orderDate = null;
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntity = $customerOrders->last();
        $date = $orderEntity->getOrderDate();
        if ($date !== null) {
            $orderDate = $date->format('Y-m-d H:i:s');
        }
        return $orderDate;
    }

    /**
     * @param int $customerId
     * @return array|null
     * @author Pradeep
     */
    public function getSmartTags(int $customerId): ?string
    {
        $smartTags = '';
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntity = $customerOrders->first();
        if($orderEntity->getLineItems() === null) {
            return null;
        }
        foreach ($orderEntity->getLineItems()->getElements() as $item) {
            $product = $item->getProduct();
            $categories = $product->getCategories();
            foreach ($categories as $category) {
                $tags = $category->getBreadcrumb();
            }
        }
        if (!empty($tags)) {
            $smartTags = implode(', ', $tags);
        }
        return $smartTags;
    }

    /**
     * @param int $customerId
     * @return float|null
     * @author Pradeep
     */
    public function getTotalOrderNetValue(int $customerId): ?float
    {
        $netValue = 0;
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntities = $customerOrders->getElements();
        foreach ($orderEntities as $orderEntity) {
            $orderNetValue = $orderEntity->getPrice()->getNetPrice();
            $netValue += (float)$orderNetValue;
        }
        return round($netValue, 2);
    }

    /**
     * @param int $customerId
     * @return int|null
     * @author Pradeep
     */
    public function getLastOrderNumber(int $customerId): ?int
    {
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntities = $customerOrders->getElements();

        foreach ($orderEntities as $orderEntity) {
            $orderNumbers[] = (int)$orderEntity->getOrderNumber();
        }
        sort($orderNumbers, SORT_NUMERIC);
        $count = count($orderNumbers);
        return $orderNumbers[ $count - 1 ];
    }

    /**
     * @param int $customerId
     * @return float|null
     * @author Pradeep
     */
    public function getLastOrderNetValue(int $customerId): ?float
    {
        $lastOrderNetValue = null;
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntity = $customerOrders->first();
        $netValue = $orderEntity->getPrice()->getNetPrice();
        if (!empty($netValue)) {
            $lastOrderNetValue = (float)$netValue;
        }
        return $lastOrderNetValue;
    }

    /**
     * @param int $customerId
     * @return float|null
     * @author Pradeep
     */
    public function getLastYearOrderNetValue(int $customerId): ?float
    {
        $oneYear = 1;
        $lastYearNetValue = null;
        $customerOrders = $this->getCustomerOrders($customerId);
        if ($customerOrders === null) {
            return null;
        }
        $orderEntities = $customerOrders->getElements();

        $currentDate = new DateTime("now");
        $currentYear = $currentDate->format('Y');

        foreach ($orderEntities as $orderEntity) {
            $orderDate = $orderEntity->getOrderDate();
            $orderedYear = $orderDate->format('Y');
            $diff = (int)$currentYear - (int)$orderedYear;
            if ($diff > $oneYear) {
                continue;
            }
            $lastYearNetValue += (float)$orderEntity->getPrice()->getNetPrice();
        }

        return $lastYearNetValue;
    }

}
