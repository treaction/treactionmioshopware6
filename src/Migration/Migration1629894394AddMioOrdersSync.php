<?php declare(strict_types=1);

namespace Treaction\MIO\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1629894394AddMioOrdersSync extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1629894394;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS`mio_orders_sync` (
              `id` BINARY(16) NOT NULL,
              `mio_account_number` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
              `last_sync_time_stamp` DATETIME NOT NULL,
              `last_sync_status` TINYINT(1) NOT NULL,
              `last_order_id` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
              `first_order_id` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
              `orders_count` TINYINT(11) NOT NULL,
               PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
