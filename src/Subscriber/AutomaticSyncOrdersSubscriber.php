<?php

namespace Treaction\MIO\Subscriber;

use Shopware\Core\Content\Newsletter\Event\NewsletterRegisterEvent;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityDeletedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Plugin\Event\PluginPostActivateEvent;
use Shopware\Core\Framework\Plugin\PluginEvents;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\PageLoadedEvent;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Treaction\MIO\MIOClient\TestAPIKey;
use Treaction\MIO\MIOClient\Webhooks\NewsletterHook;
use Treaction\MIO\Repository\CustomFieldRepository;
use Treaction\MIO\Service\PluginLogger;

class AutomaticSyncOrdersSubscriber implements EventSubscriberInterface
{

    /**
     * @var SystemConfigService
     * @author Pradeep
     */
    private $systemConfigService;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var TestAPIKey
     * @author Pradeep
     */
    private $testApiKey;

    public function __construct(
        SystemConfigService $systemConfigService,
        PluginLogger $logger
    ) {
        $this->systemConfigService = $systemConfigService;
        $this->testApiKey = new TestAPIKey();
        $this->logger = $logger;
        $this->logger->addLog('info', 'AutomaticSyncOrdersSubscriber');
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductEvents::PRODUCT_LOADED_EVENT => 'onProductsLoaded',
        ];
    }

    public function onProductsLoaded(EntityLoadedEvent $event)
    {
        /*       $response =  $this->testApiKey->validateAPIKeyAccountNumber($this->getAPIKey(), $this->getAccountNumber());

               $this->systemConfigService->set('TreactionMIOShopware6.config.shopUrl', $response['response']['company']['website']) ;
               $this->systemConfigService->set('TreactionMIOShopware6.config.shopName', $response['response']['company']['name']) ;
               $this->systemConfigService->set('TreactionMIOShopware6.config.optinurl',$response['response']['company']['url_privacy_policy']) ;

               $context = $event->getContext();

               $t = $this->customFieldRepository->getPluginEntity('TreactionMIOShopware6', $context);
               // set Privacy Policy.
               $this->customFieldRepository->set($t,'privacy_policy_url',$response['response']['company']['url_privacy_policy'],
               $context);

               // set Privacy Policy.
               $this->customFieldRepository->set($t,'imprint_url',$response['response']['company']['url_imprint'],
                   $context);



               // set Privacy Policy.
               $this->customFieldRepository->set($t,'website',$response['response']['company']['website'],
                   $context);

               $t2 = $this->customFieldRepository->getPluginEntity('TreactionMIOShopware6', $context);
               $cus2 = $t2->getCustomFields();
               $this->logger->addLog('info', 'CustomFields 2'.json_encode($cus2));*/
        return true;
    }


    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAPIKey(): ?string
    {
        return trim($this->systemConfigService->get('TreactionMIOShopware6.config.apikey'));
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAccountNumber(): ?string
    {
        return (int)trim($this->systemConfigService->get('TreactionMIOShopware6.config.accountno'));
    }
}