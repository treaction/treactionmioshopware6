<?php

namespace Treaction\MIO\Subscriber;

use Shopware\Core\Content\Newsletter\Aggregate\NewsletterRecipient\NewsletterRecipientEntity;
use Shopware\Core\Content\Newsletter\Event\NewsletterConfirmEvent;
use Shopware\Core\Content\Newsletter\Event\NewsletterRegisterEvent;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Treaction\MIO\DataProvider\SalutationProvider;
use Treaction\MIO\MIOClient\Webhooks\NewsletterHook;
use Treaction\MIO\Service\PluginLogger;
use Treaction\MIO\Service\Validator;


class NewsletterSubscriber implements EventSubscriberInterface
{

    private const EVENT_NAME = 'newsletter.register';
    /**
     * @var SystemConfigService
     * @author Pradeep
     */
    private $systemConfigService;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $newsletterRecipientRepository;
    /**
     * @var Validator
     * @author Pradeep
     */
    private $validator;
    /**
     * @var NewsletterHook
     * @author Pradeep
     */
    private $newsLetterHook;
    /**
     * @var SalutationProvider
     * @author Pradeep
     */
    private $salutationProvider;

    /**
     * @param SystemConfigService $symtemConfigService
     */
    public function __construct(
        SystemConfigService $symtemConfigService,
        NewsletterHook $newsletterHook,
        SalutationProvider $salutationProvider
    ) {
        $this->systemConfigService = $symtemConfigService;
        $this->logger = new PluginLogger();
        $this->newsLetterHook = $newsletterHook;
        $this->salutationProvider = $salutationProvider;
        $this->logger->addLog('info', 'Constructor ', __CLASS__, __METHOD__, __LINE__);
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    public static function getSubscribedEvents()
    {
        return [
            NewsletterRegisterEvent::class => 'register',
            NewsletterConfirmEvent::class => 'onNewsletterConfirmEvent',
        ];
    }

    /**
     * @param NewsletterRegisterEvent $event
     * @return bool
     * @author Pradeep
     */
    public function register(NewsletterRegisterEvent $event): bool
    {
        $eventName = $event->getName();
        $context = $event->getContext();

        $this->salutationProvider->setContext($context);
        if ($eventName === self::EVENT_NAME) {
            // NewsletterRecipient is not an array instead it is an Entity Object.
            $recipient = $event->getNewsletterRecipient();
            if ($recipient === null) {
                return false;
            }
            if(!$this->newsLetterHook->setAccountNumber($this->getAccountNumber()) ||
               !$this->newsLetterHook->setAPIKey($this->getAPIKey())) {
                $this->logger->addLog('error', 'Failed '
                                               . json_encode([
                        'apikey ' => $this->getAPIKey(),
                        'accNo' => $this->getAccountNumber(),
                    ]));
                return false;
            }

            $this->newsLetterHook->send($this->generatePayload($recipient));
            //$newsLetterHook->sendToNLWebhook($recipient);
        }
        return true;
    }

    /**
     * @param NewsletterRecipientEntity $recipient
     * @return array
     * @author Pradeep
     */
    private function generatePayload(NewsletterRecipientEntity $recipient)
    {
        return [
            'email'=>$recipient->getEmail() ?? '',
            'salutation' => $this->salutationProvider->get($recipient->getSalutationId()) ?? '',
            'firstName'=> $recipient->getFirstName() ?? '',
            'lastName' => $recipient->getLastName() ?? ''
        ];
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAPIKey():?string
    {
        return trim($this->systemConfigService->get('TreactionMIOShopware6.config.apikey'));
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAccountNumber():?string
    {
        return (int)trim($this->systemConfigService->get('TreactionMIOShopware6.config.accountno'));
    }

}