<?php declare(strict_types=1);

namespace Treaction\MIO\Subscriber;


use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;
use Shopware\Core\Checkout\Cart\Exception\OrderNotFoundException;
use Shopware\Core\Checkout\Order\Aggregate\OrderCustomer\OrderCustomerEntity;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Content\Category\CategoryCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\Checkout\Finish\CheckoutFinishPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Framework\Context;
use Treaction\MIO\DataProvider\CustomerOrderProvider;
use Treaction\MIO\DataProvider\OrderProvider;
use Treaction\MIO\MIOClient\Webhooks\ECommerceHook;
use Treaction\MIO\Service\PluginLogger;
use Symfony\Component\HttpFoundation\RequestStack;

class EcommerceSubscriber implements EventSubscriberInterface
{
    /**
     * @var SystemConfigService
     * @author Pradeep
     */
    private $systemConfigService;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $orderRepository;
    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $orderCustomerRepository;
    /**
     * @var EntityRepositoryInterface
     * @author Pradeep
     */
    private $categoryRepository;
    /**
     * @var iterable
     * @author Pradeep
     */
    private $demoDataProvider;
    /**
     * @var CustomerOrderProvider
     * @author Pradeep
     */
    private $customerOrderProvider;
    /**
     * @var ECommerceHook
     * @author Pradeep
     */
    private $eCommerceHook;

    /**
     * @param SystemConfigService $symtemConfigService
     */
    public function __construct(
        SystemConfigService $symtemConfigService,
        OrderProvider $orderProvider,
        CustomerOrderProvider $customerOrderProvider,
        ECommerceHook $eCommerceHook,
        RequestStack $requestStack
    ) {
        $this->systemConfigService = $symtemConfigService;
        $this->orderProvider = $orderProvider;
        $this->customerOrderProvider = $customerOrderProvider;
        $this->eCommerceHook = $eCommerceHook;
        $this->requestStack = $requestStack;
        $this->logger = new PluginLogger();
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CheckoutFinishPageLoadedEvent::class => 'onCheckOutFinishPageLoaded',
            CheckoutOrderPlacedEvent::class => 'onFinishPage',
        ];
    }

    public function onFinishPage(CheckoutOrderPlacedEvent $event) :bool
    {
        // get order event

        if ($this->isPermissionGranted()) {
            $orderEntity = $event->getOrder();
            $context = $event->getContext();
            $orderDetails = $this->getOrderDetails($context, $orderEntity);
            if (!$this->eCommerceHook->setAPIKey($this->getAPIKey()) ||
                !$this->eCommerceHook->setAccountNumber($this->getAccountNumber())) {
                return false;
            }
            if (!$this->eCommerceHook->send($orderDetails)) {
                return false;
            }
        }

        return true;
    }

    private function getOrderDetails(Context $context, OrderEntity $order)
    {
        $orderDetails = [];
        if ($order->getOrderCustomer() === null) {
            return $orderDetails;
        }
        $customerId = (int)$order->getOrderCustomer()->getCustomerNumber();
        $this->orderProvider->setContext($context);
        $orderDetails = $this->orderProvider->getUserInformation($customerId);
        $orderDetails[ 'lastOrderDate' ] = $this->orderProvider->getLastOrderData($customerId);
        $orderDetails[ 'lastOrderNumber' ] = $this->orderProvider->getLastOrderNumber($customerId);
        $orderDetails[ 'totalOrderNetValue' ] = $this->orderProvider->getTotalOrderNetValue($customerId);
        $orderDetails[ 'lastOrderNetValue' ] = $this->orderProvider->getLastOrderNetValue($customerId);
        $orderDetails[ 'lastYearOrderNetValue' ] = $this->orderProvider->getLastYearOrderNetValue($customerId);
        $orderDetails[ 'smartTags' ] = $this->orderProvider->getSmartTags($customerId);

        $this->logger->addLog('info', 'order details', json_encode($orderDetails));

        return $orderDetails;
    }

    public function onCheckOutFinishPageLoaded(CheckoutFinishPageLoadedEvent $event)
    {
        $permission = $this->isPermissionGranted();
        $this->logger->addLog('info', 'onFinishPage ' . json_encode($permission));

    }

    public function isPermissionGranted(): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        $permission = $request->get('opt-out-text');
        return $permission === 1 || $permission === '1' || $permission === "on";
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAPIKey(): ?string
    {
        return trim($this->systemConfigService->get('TreactionMIOShopware6.config.apikey'));
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    private function getAccountNumber():?int
    {
        return (int)trim($this->systemConfigService->get('TreactionMIOShopware6.config.accountno'));
    }
}