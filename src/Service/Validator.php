<?php

namespace Treaction\MIO\Service;

use Exception;

class Validator
{
    private CONST MIN_STR_LENGTH = 3;
    private CONST MAX_STR_LENGTH = 255;
    private CONST START_ACCOUNT_NUMBER = 1000;

    public function __construct()
    {
        $this->logger = new PluginLogger();
    }

    /**
     * @param string $email
     * @return bool
     * @author Pradeep
     */
    public function isValidEmail(string $email): bool
    {
        $sanitizedEmail = $this->sanitizeEmail($email);
        if(!filter_var($sanitizedEmail, FILTER_VALIDATE_EMAIL)) {
            $this->logger->addLog('error', 'Invalid Email ', __CLASS__, __METHOD__,__LINE__);
            return false;
        }
        return true;
    }

    /**
     * @param int $integer
     * @return bool
     * @author Pradeep
     */
    public function isValidInteger(int $integer): bool
    {
        if(!filter_var($integer, FILTER_VALIDATE_INT)) {
            $this->logger->addLog('error', 'Invalid Email ', __CLASS__, __METHOD__,__LINE__);
            return false;
        }
        return true;
    }

    /**
     * @param string $email
     * @return string
     * @author Pradeep
     */
    public function sanitizeEmail(string $email): string
    {
        if(empty($email)) {
            return '';
        }
        return filter_var($email, FILTER_SANITIZE_EMAIL);
    }

    /**
     * @param string $string
     * @return string
     * @author Pradeep
     */
    public function sanitizeString(string $string): string
    {
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    /**
     * @param string $salutation
     * @return bool
     * @author Pradeep
     */
    public function isValidSalutation(string $salutation): bool
    {
        $supportedSalutations = ['Herr', 'herr', 'Frau', 'frau' ,'MR', 'Mr', 'mr', 'MRS', 'Mrs', 'mrs'];
        return in_array($salutation, $supportedSalutations);
    }

    /**
     * @param string $name
     * @return bool
     * @author Pradeep
     * @internal Validation for First, Middle and Last Name.
     */
    public function isValidName(string $name):bool
    {
        try {
            if (!$this->isValidLength($name)) {
                throw new Exception('Invalid Size, Name is either too short or too long.');
            }
            if($this->hasSpecialCharacters($name)) {
                throw new Exception('Special characters are not allowed.');
            }
            return true;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__,__LINE__);
            return false;
        }
    }

    /**
     * @param int $accountNumber
     * @return bool
     * @author Pradeep
     * @internal MIO Account Number always starts from '1000'
     */
    public function isValidAccountNumber(int $accountNumber):bool
    {
        try {
            if (!$this->isValidInteger($accountNumber)) {
                throw new Exception('Invalid Account Number should always be an interger value');
            }
            if($accountNumber < (int)self::START_ACCOUNT_NUMBER) {
                throw new Exception('Invalid Account Number');
            }
            return true;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__,__LINE__);
            return false;
        }
    }

    /**
     * @param string $apiKey
     * @return bool
     * @author Pradeep
     */
    public function isValidAPIKey(string $apiKey):bool
    {
        try {
            if (!$this->isValidLength($apiKey)) {
                throw new Exception('Invalid Size, APIKey is either too short or too long.');
            }
            if($this->hasSpecialCharacters($apiKey)) {
                throw new Exception('Special characters are not allowed.');
            }
            return true;
        } catch (Exception $e) {
            $this->logger->addLog('exception',$e->getMessage(), __CLASS__,__METHOD__,__LINE__);
            return false;
        }
    }

    private function isValidLength(string $string) : bool
    {
        $length = strlen($string);
        return !($length <= self::MIN_STR_LENGTH || $length > self::MAX_STR_LENGTH);
    }

    public function isValidPermission(string $permission): bool
    {
        return true;
    }

    private function hasSpecialCharacters(string $string): bool
    {
        return preg_match('/'.preg_quote('^\'£$%^&*()}{@#~?><,@|-=-_+-¬', '/').'/', $string);
    }
}