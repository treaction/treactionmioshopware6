<?php

namespace Treaction\MIO\Service;

use DateTime;

class PluginLogger
{

    private CONST LOG_FILE_PATH = __DIR__.'/../../var/dev.log';
    /**
     * @var DateTime
     * @author Pradeep
     */
    private $dateTime;

    public function __construct()
    {
        $this->dateTime = new DateTime();
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getLogLocation():string {
        return self::LOG_FILE_PATH;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getTimeStamp():string {
        $unixTimeStamp = $this->dateTime->getTimestamp();
        $this->dateTime->setTimestamp($unixTimeStamp);
        return $this->dateTime->format('Y-m-d H:i:s');
    }

    /**
     * @param string $type
     * @param string $message
     * @param string $className
     * @param string $method
     * @param string $line
     * @author Pradeep
     */
    public function addLog(string $type, string $message, string $className='', string $method='', string $line=''):void {
        $destination = $this->getLogLocation();
        $currentTime = $this->getTimeStamp();
        $message = '['.$currentTime.']'.$type.':'.$message.'['.$className.','.$method.','.$line.']';
        error_log(print_r($message,true)."\n", 3, $destination);
    }
}
/*$p = new PluginLogger();
$p->addLog('test','test');*/