import template from './treaction-module-site.html.twig';
import './treaction-module-site.scss';

const { Component, Mixin } = Shopware;

Component.register('treaction-module-site', {
	template,
	
	/*inject: ['TreactionImportApiService'],*/
	
	mixins: [
		Mixin.getByName('notification')
	],
	
	metaInfo() {
		return {
			title: this.$createTitle()
		};
	},
	
	data() {
		return {
			permission: null,
			permissions: [
				{ value: 1, label: 'None' },
				{ value: 2, label: 'Single-Opt-In' },
				{ value: 3, label: 'Confirmed-Opt-In' },
				{ value: 4, label: 'Double-Opt-In' },
				{ value: 5, label: 'Double-Opt-In Plus' },
				{ value: 6, label: 'Other' }
			]
		};
	},
	
	methods: {
		onClickImport() {
			var data = { "permission": this.permission }
			
			this.TreactionImportApiService.saveConfig(data)
				.then(response => {
					if (response.success) {
						this.createNotificationSuccess({
							title: this.$tc('treaction-module.messages.messageTitle'),
							message: response.msg
						});
					} else {
						this.createNotificationError({
							title: this.$tc('treaction-module.messages.errorMessageTitle'),
							message: response.msg
						});
					}
				})
				.catch(err => {
					this.createNotificationError({
						title: this.$tc('treaction-module.messages.errorMessageTitle'),
						message: err.message
					});
				});
		}
	}
});
