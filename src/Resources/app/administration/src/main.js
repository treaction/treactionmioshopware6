/* Components */
import './component/treaction-api-test-button';
import './component/treaction-acc-register-button';
import './component/treaction-order-sync-button';

/* Services */
import './service/treactionMIOOrderSyncService'
import './service/treactionMIOApiTestService'
import './service/treactionMIOAccRegisterService'


import localeDE from './snippet/de_DE.json';
import localeEN from './snippet/en_GB.json';
Shopware.Locale.extend('de-DE', localeDE);
Shopware.Locale.extend('en-GB', localeEN);