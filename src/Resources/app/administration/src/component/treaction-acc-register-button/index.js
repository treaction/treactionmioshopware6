const { Component, Mixin } = Shopware;
import template from './treaction-acc-register-button.html.twig';

Component.register('treaction-acc-register-button', {
    template: template,

    props: ['label'],
    inject: ['treactionAccRegister'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    computed: {
        pluginConfig() {
            let email=  document.getElementById('TreactionMIOShopware6.config.email').value;
            let firstname = document.getElementById('TreactionMIOShopware6.config.firstname').value;
            let lastname = document.getElementById('TreactionMIOShopware6.config.lastname').value;
            let salutation = document.getElementsByClassName('sw-single-select__selection-text')[0].innerText
            return [{'email' :email,'firstname':firstname,'lastname':lastname, 'salutation':salutation} ]
        }
    },

    methods: {

        saveFinish() {
            this.isSaveSuccessful = false;
        },

        check() {
            this.isLoading = true;
            var data = this.pluginConfig;

            this.treactionAccRegister.check(data).then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('treaction-acc-register-button.title'),
                        message: this.$tc('treaction-acc-register-button.success')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('treaction-acc-register-button.title'),
                        message: this.$tc('treaction-acc-register-button.error'),
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
