const { Component, Mixin } = Shopware;
import template from './treaction-api-test-button.html.twig';

Component.register('treaction-api-test-button', {
	template: template,

	props: ['label'],
	inject: ['treactionApiTest'],

	mixins: [
		Mixin.getByName('notification')
	],

	data() {
		return {
			isLoading: false,
			isSaveSuccessful: false,
		};
	},

	computed: {
		pluginConfig() {
			let apikey=  document.getElementById('TreactionMIOShopware6.config.apikey').value;
            let accountNo = document.getElementById('TreactionMIOShopware6.config.accountno').value;
			document.getElementById('').return [{ 'apikey': apikey, 'accountNo': accountNo }]
		}
	},

	methods: {

		saveFinish() {
			this.isSaveSuccessful = false
			this.$router.go()
		},

		check() {
			this.isLoading = true;
			var data = this.pluginConfig

			this.treactionApiTest.check(data).then((res) => {
				if (res.success) {
					this.isSaveSuccessful = true;
					this.createNotificationSuccess({
						title: this.$tc('treaction-api-test-button.title'),
						message: this.$tc('treaction-api-test-button.success'),
					});
					//document.getElementById('TreactionMIOShopware6.config.shopName').setAttribute('value','testShop');
				} else {
					this.createNotificationError({
						title: this.$tc('treaction-api-test-button.title'),
						message: this.$tc('treaction-api-test-button.error'),
					});
				}

				this.isLoading = false;
			});
		}
	}
})
