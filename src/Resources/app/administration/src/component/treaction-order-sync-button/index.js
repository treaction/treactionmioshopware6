const { Component, Mixin } = Shopware;
import template from './treaction-order-sync-button.html.twig';

Component.register('treaction-order-sync-button', {
    template: template,

    props: ['label'],
    inject: ['treactionOrderSync'],

    mixins: [
        Mixin.getByName('notification')
    ],

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
        };
    },

    computed: {
        pluginConfig() {
            let apikey=  document.getElementById('TreactionMIOShopware6.config.apikey').value;
            let accountNo = document.getElementById('TreactionMIOShopware6.config.accountno').value;
            return [{'apikey' :apikey,'accountNo':accountNo} ]
        }
    },

    methods: {

        saveFinish() {
            this.isSaveSuccessful = false;
        },

        check() {
            this.isLoading = true;
            var data = this.pluginConfig;

            this.treactionOrderSync.check(data).then((res) => {
                if (res.success) {
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$tc('treaction-order-sync-button.title'),
                        message: this.$tc('treaction-order-sync-button.success')
                    });
                } else {
                    this.createNotificationError({
                        title: this.$tc('treaction-order-sync-button.title'),
                        message: this.$tc('treaction-order-sync-button.error'),
                    });
                }

                this.isLoading = false;
            });
        }
    }
})
