const ApiService = Shopware.Classes.ApiService;
const { Application } = Shopware;

class ApiClient extends ApiService {
	constructor(httpClient, loginService, apiEndpoint = 'treaction-order-sync') {
		super(httpClient, loginService, apiEndpoint);
	}
	
	check(values) {
		const headers = this.getBasicHeaders({});
		
		return this.httpClient
			.post(`${this.getApiBasePath()}/sync`, values,{
				headers
			})
			.then((response) => {
				return ApiService.handleResponse(response);
			});
	}
}

Application.addServiceProvider('treactionOrderSync', (container) => {
	const initContainer = Application.getContainer('init');
	return new ApiClient(initContainer.httpClient, container.loginService);
});