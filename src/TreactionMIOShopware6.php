<?php
namespace Treaction\MIO;

use Doctrine\DBAL\Connection;
use RuntimeException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class TreactionMIOShopware6 extends Plugin
{

    public function install(InstallContext $context): void
    {
        parent::install($context);
    }

    public function uninstall(UninstallContext $context): void
    {


        parent::uninstall($context);
        if(!$context->keepUserData()) {
            $connection = $this->getConnection();
            $connection->exec('DROP TABLE IF EXISTS `mio_orders_sync`');
            $connection->exec("DELETE FROM system_config WHERE configuration_key LIKE '%TreactionMIOShopware6%'");
        }
    }

    private function getConnection(): Connection
    {
        /** @var Connection $connection */
        $connection = $this->container->get(Connection::class);

        if (!$connection instanceof Connection) {
            throw new RuntimeException('Expected connection service to be initialized');
        }

        return $connection;
    }
}