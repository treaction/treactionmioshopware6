<?php declare(strict_types=1);

namespace Treaction\MIO\Core;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class MioOrderSyncEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string | null
     */
    protected $mioAccountNumber;

    /**
     * @var \DateTimeInterface | null
     */
    protected $lastSyncTimeStamp;

    /**
     * @var int | null
     */
    protected $lastSyncStatus;

    /**
     * @var string | null
     */
    protected $lastOrderId;

    /**
     * @var string | null
     */
    protected $firstOrderId;

    /**
     * @var int | null
     */
    protected $ordersCount;

    public function setMioAccountNumber(?string $mioAccountNumber):void
    {
        $this->mioAccountNumber = $mioAccountNumber;
    }

    public function getMioAccountNumber(): ?string
    {
        return $this->mioAccountNumber;
    }

    public function setLastSyncTimeStamp(?\DateTimeInterface $lastSyncTimeStamp):void
    {
        $this->lastSyncTimeStamp = $lastSyncTimeStamp;
    }

    public function getLastSyncTimeStamp():?\DateTimeInterface
    {
        return $this->lastSyncTimeStamp;
    }

    public function setLastSyncStatus(?int $lastSyncStatus):void
    {
        $this->lastSyncStatus = $lastSyncStatus;
    }

    public function getLastSyncStatus():?int
    {
        return $this->lastSyncStatus;
    }

    public function setLastOrderId(?string $lastOrderId):void
    {
        $this->lastOrderId = $lastOrderId;
    }

    public function getLastOrderId():?string
    {
        return $this->lastOrderId;
    }

    public function setFirstOrderId(?string $firstOrderId):void
    {
        $this->firstOrderId = $firstOrderId;
    }

    public function getFirstOrderId():?string
    {
        return $this->firstOrderId;
    }

    public function setOrdersCount(?int $ordersCount):void
    {
        $this->ordersCount = $ordersCount;
    }

    public function getOrdersCount():?int
    {
        return $this->ordersCount;
    }

}
