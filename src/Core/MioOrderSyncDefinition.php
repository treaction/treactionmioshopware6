<?php declare(strict_types=1);

namespace Treaction\MIO\Core;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class MioOrderSyncDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'mio_orders_sync';
    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
        //return MioOrderSyncEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new StringField('mio_account_number', 'mioAccountNumber')),
            (new DateField('last_sync_time_stamp', 'lastSyncTimeStamp')),
            (new IntField('last_sync_status', 'lastSyncStatus')),
            (new StringField('last_order_id', 'lastOrderId')),
            (new StringField('first_order_id', 'firstOrderId')),
            (new IntField('orders_count', 'ordersCount')),
        ]);
    }
}