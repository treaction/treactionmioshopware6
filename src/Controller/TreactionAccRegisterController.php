<?php

namespace Treaction\MIO\Controller;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Treaction\MIO\MIOClient\Account\UserAccount;
use Treaction\MIO\MIOClient\TestAPIKey;
use Treaction\MIO\Service\PluginLogger;
use Shopware\Core\System\SystemConfig\SystemConfigService;

/**
 * @RouteScope(scopes={"api"})
 */
class TreactionAccRegisterController
{
    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
        $this->logger = new PluginLogger();
        $this->account = new UserAccount();
    }

    /**
     * @Route("/api/v{version}/treaction-acc-register/create",
     * name="api.action.treaction.acccreate", methods={"POST"})
     */
    public function create(Request $request): JsonResponse
    {
        $content = $request->getContent();
        $contentAsArr = json_decode($content, true);

        $salutation = trim($contentAsArr[ 0 ][ 'salutation' ]);
        $firstname = trim($contentAsArr[ 0 ][ 'firstname' ]);
        $lastname = trim($contentAsArr[ 0 ][ 'lastname' ]);
        $email = trim($contentAsArr[ 0 ][ 'email' ]);
        $permission = true;//$contentAsArr[0]['permission'];

        $this->systemConfigService->set('TreactionMIOShopware6.config.salutation', $salutation);
        $this->systemConfigService->set('TreactionMIOShopware6.config.firstname', $firstname);
        $this->systemConfigService->set('TreactionMIOShopware6.config.lastname', $lastname);
        $this->systemConfigService->set('TreactionMIOShopware6.config.email', $email);

        $response = $this->account->create($salutation, $firstname, $lastname, $email, $permission);
        $this->logger->addLog('info', 'create Controller', json_encode($response));
        return new JsonResponse(['success' => 'reached Acc Create controller']);
    }
}
