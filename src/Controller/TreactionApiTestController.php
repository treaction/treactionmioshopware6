<?php declare(strict_types=1);

namespace Treaction\MIO\Controller;

use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Treaction\MIO\MIOClient\TestAPIKey;
use Treaction\MIO\Repository\CustomFieldRepository;
use Treaction\MIO\Service\PluginLogger;

/**
 * @RouteScope(scopes={"api"})
 */
class TreactionApiTestController
{
    /**
     * @var SystemConfigService
     * @author Pradeep
     */
    private $systemConfigService;
    /**
     * @var CustomFieldRepository
     * @author Pradeep
     */
    private $customFieldRepository;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
        $this->logger = new PluginLogger();
        $this->test = new TestAPIKey();
    }

    /**
     * @Route("/api/v{version}/treaction-api-test/verify",
     * name="api.action.treaction.apitest", methods={"POST"})
     */
    public function check(Request $request): JsonResponse
    {
        $content = $request->getContent();
        $contentAsArr = json_decode($content, true);

        if (!isset($contentAsArr[ 0 ][ 'apikey' ], $contentAsArr[ 0 ][ 'accountNo' ]) ||
            empty($contentAsArr[ 0 ][ 'apikey' ]) || empty($contentAsArr[ 0 ][ 'accountNo' ])
        ) {
            return new JsonResponse([
                'success' => false,
                'msg' => 'Please save Plugin configuration before Testing the APIkey',
            ]);
        }
        $apikey = trim($contentAsArr[ 0 ][ 'apikey' ]);
        $accNumber = trim($contentAsArr[ 0 ][ 'accountNo' ]);

        if (empty($apikey) || empty($accNumber)) {
            return new JsonResponse([
                'success' => false,
                'msg' => 'Please save Plugin configuration before Testing the APIkey',
            ]);
        }

        $response = $this->test->validateAPIKeyAccountNumber($apikey, (int)$accNumber);

        if (!isset($response[ 'response' ][ 'company' ]) || empty($response[ 'response' ][ 'company' ])) {
            return new JsonResponse(['success' => false, 'msg' => $response[ 'message' ]]);
        }

        // Save in system_config.
        // System_config is used to show in UI.
        // all these entries in system_config are deleted while uninstalling the plugin.
        $this->systemConfigService->set('TreactionMIOShopware6.config.apikey', $apikey);
        $this->systemConfigService->set('TreactionMIOShopware6.config.accountno', $accNumber);
        $this->systemConfigService->set('TreactionMIOShopware6.config.shopUrl',
            "https://" . $response[ 'response' ][ 'company' ][ 'website' ]);
        $this->systemConfigService->set('TreactionMIOShopware6.config.shopName',
            $response[ 'response' ][ 'company' ][ 'name' ]);
        $this->systemConfigService->set('TreactionMIOShopware6.config.optinurl',
            $response[ 'response' ][ 'company' ][ 'url_privacy_policy' ]);
        $this->systemConfigService->set('TreactionMIOShopware6.config.imprint',
            $response[ 'response' ][ 'company' ][ 'url_imprint' ]);

        return new JsonResponse(['success' => $response[ 'status' ], 'msg' => $response[ 'message' ]]);
    }


    private function getContext()
    {
        return Context::createDefaultContext();
    }
}
