<?php

namespace Treaction\MIO\Controller;

use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Treaction\MIO\Core\MioOrderSyncDefinition;
use Treaction\MIO\DataProvider\OrderProvider;
use Treaction\MIO\MIOClient\Account\UserAccount;
use Treaction\MIO\MIOClient\Webhooks\OrderSynchronization;
use Treaction\MIO\Repository\MioOrderSyncRepository;
use Treaction\MIO\Service\PluginLogger;

/**
 * @RouteScope(scopes={"api"})
 */
class TreactionOrderSyncController
{
    /**
     * @var OrderSynchronization
     * @author Pradeep
     */
    private $orderSynchronization;
    /**
     * @var OrderProvider
     * @author Pradeep
     */
    private $orderProvider;
    /**
     * @var PluginLogger
     * @author Pradeep
     */
    private $logger;
    /**
     * @var MioOrderSyncRepository
     * @author Pradeep
     */
    private $mioOrderSyncRepository;

    public function __construct(
        OrderSynchronization $orderSynchronization,
        OrderProvider $orderProvider,
        MioOrderSyncRepository $mioOrderSyncRepository,
        PluginLogger $pluginLogger
    ) {
        $this->orderSynchronization = $orderSynchronization;
        $this->orderProvider = $orderProvider;
        $this->mioOrderSyncRepository = $mioOrderSyncRepository;
        $this->logger = $pluginLogger;
    }

    /**
     * @Route("/api/v{version}/treaction-order-sync/sync",
     * name="api.action.treaction.ordersync", methods={"POST"})
     */
    public function sync(Request $request): JsonResponse
    {

        $this->logger->addLog('info', 'sync ');
        $content = $request->getContent();
        $contentAsArr = json_decode($content, true);

        if (!isset($contentAsArr[ 0 ][ 'apikey' ], $contentAsArr[ 0 ][ 'accountNo' ]) ||
            empty($contentAsArr[ 0 ][ 'apikey' ]) || empty($contentAsArr[ 0 ][ 'accountNo' ])
        ) {
            return new JsonResponse([
                'success' => false,
                'msg' => 'Please save Plugin configuration before Testing the APIkey',
            ]);
        }

        $apikey = trim($contentAsArr[ 0 ][ 'apikey' ]);
        $accNumber = trim($contentAsArr[ 0 ][ 'accountNo' ]);

        if (empty($apikey) || empty($accNumber)) {
            return new JsonResponse([
                'success' => false,
                'msg' => 'Please save Plugin configuration before Testing the APIkey',
            ]);
        }

        $this->logger->addLog('info', 'pluginConfig ' . json_encode(['apikey' => $apikey, 'accountNo' => $accNumber]));
        $context = $this->getContext();

        // todo duplicate setContext
        $this->orderProvider->setContext($context);

        $recentOrderNumber = $this->orderProvider->getRecentShopOrderNumber();
        $mioSync = $this->mioOrderSyncRepository->get((int)$accNumber);
        $lastSyncOrderNumber = $mioSync[ 0 ][ 'last_order_id' ] ?? 0;

        if (empty($mioSync)) {
            $history = $this->getShopOrderHistory($context);
        } elseif ($lastSyncOrderNumber < $recentOrderNumber) {
            $history = $this->getShopOrderHistory($context, $lastSyncOrderNumber);
        } else {
            return new JsonResponse(['success' => true, 'msg' => 'All records are synchronized']);
        }
/*        $this->logger->addLog('info', 'history '.json_encode($history));*/
        if (!$this->orderSynchronization->setAPIKey($apikey) ||
            !$this->orderSynchronization->setAccountNumber((int)$accNumber)) {
            return new JsonResponse(['success' => false, 'msg' => 'Failed to get APIKey Or Account Number.']);
        }

        if (!$this->orderSynchronization->send($history)) {
            return new JsonResponse(['success' => false, 'msg' => 'Something went wrong, Unable to sync orders']);
        }

        $firstOrderId = $this->getFirstOrderId($history);
        $lastOrderId = $this->getLastOrderId($history);
        $syncStatus = true;
        $orderCount = $this->getOrderCount($history);
        $this->logger->addLog('info ', json_encode([
            'firstId' => $firstOrderId,
            'lastId' => $lastOrderId,
            'sync' => $syncStatus,
            'orderCount' => $orderCount,
        ]));
        $this->mioOrderSyncRepository->insert((int)$accNumber, $firstOrderId, $lastOrderId, $syncStatus, $orderCount);
        return new JsonResponse(['success' => 'reached Acc Create controller']);
    }

    private function getContext()
    {
        return Context::createDefaultContext();
    }

    /**
     * @return array
     * @author Pradeep
     */
    private function getShopOrderHistory(Context $context, int $fromOrderNumber = 0): array
    {
        $contacts = [];
        $this->orderProvider->setContext($context);
        $history = $this->orderProvider->getShopOrderHistory($fromOrderNumber);
        if ($history === null) {
            return $contacts;
        }
        $orders = $history->getElements();
        foreach ($orders as $order) {
            $contacts[] = $this->extractOrders($order);
        }
        //$this->logger->addLog('info', 'getshopOrderHistory ' . json_encode($contacts));
        return $contacts;
    }

    /**
     * @param OrderEntity $orderEntity
     * @return array
     * @author Pradeep
     */
    private function extractOrders(OrderEntity $orderEntity): array
    {
        $customerId = (int)$orderEntity->getOrderCustomer()->getCustomerNumber();
        $user[ 'email' ] = $orderEntity->getOrderCustomer()->getEmail();
        $user[ 'firstName' ] = $orderEntity->getOrderCustomer()->getFirstName();
        $user[ 'lastName' ] = $orderEntity->getOrderCustomer()->getLastName();
        $user[ 'salutation' ] = $orderEntity->getOrderCustomer()->getSalutation()->getDisplayName();
        $addresses = $orderEntity->getAddresses();
        foreach ($addresses as $address) {
            if ($address === null) {
                continue;
            }
            $user[ 'street' ] = $address->getStreet();
            $user[ 'hNo' ] = '';
            $user[ 'city' ] = $address->getCity();
            $user[ 'postalCode' ] = $address->getZipcode();
            $user[ 'country' ] = $address->getCountry()->getName();
        }
        $user[ 'lastOrderDate' ] = $orderEntity->getOrderDate()->format('Y-m-d H:i:s');
        $user[ 'lastOrderNumber' ] = $orderEntity->getOrderNumber();
        $user[ 'lastOrderNetValue' ] = $orderEntity->getPrice()->getNetPrice();
        $user[ 'totalOrderNetValue' ] = $this->orderProvider->getTotalOrderNetValue($customerId);
        $user[ 'lastYearOrderNetValue' ] = $this->orderProvider->getLastYearOrderNetValue($customerId);
        $user[ 'smartTags' ] = $this->orderProvider->getSmartTags($customerId);
        return $user;
    }

    /**
     * @param array $orderHistory
     * @return int|null
     * @author Pradeep
     */
    private function getFirstOrderId(array $orderHistory): ?int
    {
        $count = $this->getOrderCount($orderHistory);
        if ($count <= 0 || empty($orderHistory[ $count - 1 ])) {
            return null;
        }
        $firstOrder = $orderHistory[ $count - 1 ];
        return $firstOrder[ 'lastOrderNumber' ] ?? null;
    }

    /**
     * @param array $orderHistory
     * @return int|null
     * @author Pradeep
     */
    private function getOrderCount(array $orderHistory): ?int
    {
        if (empty($orderHistory)) {
            return null;
        }
        return count($orderHistory);
    }

    /**
     * @param array $orderHistory
     * @return int|null
     * @author Pradeep
     */
    private function getLastOrderId(array $orderHistory): ?int
    {
        if (empty($orderHistory) || empty($orderHistory[ 0 ])) {
            return null;
        }
        return $orderHistory[ 0 ][ 'lastOrderNumber' ] ?? null;
    }

}